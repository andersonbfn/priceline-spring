package com.gitlab.andersonbfn.priceline.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;

import com.gitlab.andersonbfn.priceline.dto.CreateRoleDTO;
import com.gitlab.andersonbfn.priceline.dto.RoleDTO;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class RoleService {

	/**
	 * Pra fins de praticidade e entrega do desafio mais rapido, 
	 * tratando a persistencia internamente.
	 */
	private static Set<RoleDTO> internalList;
	
	@PostConstruct
	public void init() {
		internalList = new HashSet<>();
	}
	
	public Set<RoleDTO> getAll() {
		return internalList;
	}
	
	public RoleDTO create(final CreateRoleDTO createDTO) {
		
		final RoleDTO newDTO = RoleDTO.builder()
			.withId(UUID.randomUUID().toString())
			.withName(createDTO.getName())
			.build();
		
		internalList.add(newDTO);
		
		return newDTO;
	}
	
	public RoleDTO getById(final String idRole) {
		if (idRole == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The role id must not be null.");
		}
		
		return internalList.stream()
			.filter(dto -> idRole.equals(dto.getId()))
				.findAny().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
						"The with role id '" + idRole + "' was not found."));
	}
	
}
