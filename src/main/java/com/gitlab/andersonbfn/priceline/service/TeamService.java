package com.gitlab.andersonbfn.priceline.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.gitlab.andersonbfn.priceline.dto.TeamDTO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Service
public class TeamService {

	/**
	 * Pra fins de praticidade e entrega do desafio mais rapido, 
	 * tratando a persistencia internamente.
	 */
	private static List<TeamDTO> internalList;
	
	private RestTemplate restTemplate;
	
	@Value("${priceline.api}")
	private String urlApiPriceline;
	
	@PostConstruct
	public void init() {
		internalList = new ArrayList<>();
		restTemplate = new RestTemplate();
	}
	
	public List<TeamDTO> getAll() throws JSONException {
		
		final ResponseEntity<String> responseEntity = restTemplate.getForEntity(urlApiPriceline + "/teams", String.class);
		
		if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
			final JSONArray jsonArray = new JSONArray(responseEntity.getBody());
			jsonArray.forEach(json -> {
				if (json instanceof JSONObject) {
					final JSONObject jsonObj = (JSONObject) json;
					final TeamDTO dto = TeamDTO.builder()
							.withId(jsonObj.getString("id"))
							.withName(jsonObj.getString("name"))
							.build();
					
					internalList.add(dto);
				}
			});
			
		}

		return internalList;
	}
	
	public TeamDTO getById(final String idTeam) {
		
		if (ObjectUtils.isEmpty(idTeam)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The 'team id' should be sent.");
		}
		
		final Optional<TeamDTO> optDto = internalList.stream()
			.filter(dto -> idTeam.equals(dto.getId()))
			.findAny().or(() -> {
				final ResponseEntity<String> responseEntity = restTemplate.getForEntity(urlApiPriceline + "/teams/"
						+ idTeam, String.class);
				if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
					final JSONObject jsonObj = new JSONObject(responseEntity.getBody());
					
					return Optional.of(TeamDTO.builder()
							.withId(jsonObj.getString("id"))
							.withName("name")
							.build());
				} else {
					return Optional.empty();
				}
			});
		
		optDto.ifPresent(dtoFeched -> {
			internalList.stream()
				.filter(dto -> idTeam.equals(dto.getId()))
				.findAny().ifPresent(dtoFound -> {
					dtoFound.setId(dtoFeched.getId());
					dtoFound.setName(dtoFeched.getName());
				});
		});
		
		return optDto.orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "The team for this 'id' is not found"));
	}
	
}
