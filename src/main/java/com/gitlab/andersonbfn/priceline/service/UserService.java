package com.gitlab.andersonbfn.priceline.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.annotation.PostConstruct;

import com.gitlab.andersonbfn.priceline.dto.RoleDTO;
import com.gitlab.andersonbfn.priceline.dto.TeamDTO;
import com.gitlab.andersonbfn.priceline.dto.UserDTO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserService {

	/**
	 * Pra fins de praticidade e entrega do desafio mais rapido, 
	 * tratando a persistencia internamente.
	 */
	private static Set<UserDTO> internalList;
	
	private RestTemplate restTemplate;
	
	@Value("${priceline.api}")
	private String urlApiPriceline;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	TeamService teamService;
	
	@PostConstruct
	public void init() {
		internalList = new HashSet<>();
		restTemplate = new RestTemplate();
	}
	
	public Set<UserDTO> getAll() throws JSONException {
		
		final ResponseEntity<String> responseEntity = restTemplate.getForEntity(urlApiPriceline + "/users", String.class);
		
		if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
			final JSONArray jsonArray = new JSONArray(responseEntity.getBody());
			jsonArray.forEach(json -> {
				if (json instanceof JSONObject) {
					final JSONObject jsonObj = (JSONObject) json;
					final UserDTO dto = UserDTO.builder()
							.withId(jsonObj.getString("id"))
							.withUsername(jsonObj.getString("displayName"))
							.build();
					
					internalList.add(dto);
				}
			});
			
		}

		return internalList;
	}
	
	public UserDTO getUser(final String idUser) {
		
		if (ObjectUtils.isEmpty(idUser)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The 'user id' should be sent.");
		}
		
		final Optional<UserDTO> optDto = internalList.stream()
			.filter(dto -> idUser.equals(dto.getId()))
			.findAny().or(() -> {
				final ResponseEntity<String> responseEntity = restTemplate.getForEntity(urlApiPriceline + "/users/"
						+ idUser, String.class);
				if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
					final JSONObject jsonObj = new JSONObject(responseEntity.getBody());
					
					return Optional.of(UserDTO.builder()
							.withId(jsonObj.getString("id"))
							.withUsername("displayName")
							.build());
				} else {
					return Optional.empty();
				}
			});
		
		optDto.ifPresent(dtoFeched -> {
			internalList.stream()
				.filter(dto -> idUser.equals(dto.getId()))
				.findAny().ifPresent(dtoFound -> {
					dtoFound.setId(dtoFeched.getId());
					dtoFound.setUsername(dtoFeched.getUsername());
				});
		});
		
		return optDto.orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "The user for this 'id' is not found"));
	}

	public void assignRole(final RoleDTO role, final UserDTO user) {
		if (role != null && user != null) {
			assignRole(role.getId(), user.getId());
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The role/user must not be null");
		}
	}

	public void assignRole(final String idRole, final String idUser) {
		if (idRole != null && idUser != null) {
			final Optional<UserDTO> optUser = internalList.stream()
				.filter(dto -> idUser.equals(dto.getId()))
				.findAny();
			optUser.ifPresentOrElse(dto -> {
				final RoleDTO role = roleService.getById(idRole);
				dto.setRole(role);
			}, () -> {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The user for this 'idUser' is not found");
			});
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The idRole/idUser must not be null");
		}
	}
	
	public void assignTeam(final TeamDTO team, final UserDTO user) {
		if (team != null && user != null) {
			assignTeam(team.getId(), user.getId());
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The team/user must not be null");
		}
	}
	
	public void assignTeam(final String idTeam, final String idUser) {
		if (idTeam != null && idUser != null) {
			final Optional<UserDTO> optUser = internalList.stream()
				.filter(dto -> idUser.equals(dto.getId()))
				.findAny();
			optUser.ifPresentOrElse(dto -> {
				final TeamDTO team = teamService.getById(idTeam);
				dto.setTeam(team);
			}, () -> {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The user for this 'idUser' is not found");
			});
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The idTeam/idUser must not be null");
		}
	}
}
