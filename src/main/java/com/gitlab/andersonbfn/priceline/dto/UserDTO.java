package com.gitlab.andersonbfn.priceline.dto;

import java.util.Objects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserDTO {
	
	@NotEmpty
	private String id;
	
	@NotEmpty
	private String username;
	
	@NotNull
	private TeamDTO team;

	@NotNull
	private RoleDTO role;

	private UserDTO(Builder builder) {
		this.id = builder.id;
		this.username = builder.username;
		this.team = builder.team;
		this.role = builder.role;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public TeamDTO getTeam() {
		return team;
	}

	public void setTeam(TeamDTO team) {
		this.team = team;
	}

	public RoleDTO getRole() {
		return role;
	}

	public void setRole(RoleDTO role) {
		this.role = role;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private String id;
		private String username;
		private TeamDTO team;
		private RoleDTO role;

		private Builder() {
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Builder withUsername(String username) {
			this.username = username;
			return this;
		}

		public Builder withTeam(TeamDTO team) {
			this.team = team;
			return this;
		}

		public Builder withRole(RoleDTO role) {
			this.role = role;
			return this;
		}

		public UserDTO build() {
			return new UserDTO(this);
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDTO other = (UserDTO) obj;
		return Objects.equals(id, other.id) && Objects.equals(username, other.username);
	}

	@Override
	public String toString() {
		StringBuilder builder2 = new StringBuilder();
		builder2.append("UserDTO [id=").append(id).append(", username=").append(username).append(", team=").append(team)
				.append(", role=").append(role).append("]");
		return builder2.toString();
	}
	
}
