package com.gitlab.andersonbfn.priceline.resource;

import java.util.Set;

import com.gitlab.andersonbfn.priceline.dto.CreateRoleDTO;
import com.gitlab.andersonbfn.priceline.dto.RoleDTO;
import com.gitlab.andersonbfn.priceline.service.RoleService;
import com.gitlab.andersonbfn.priceline.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/roles")
public class RoleResource {
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	UserService userService;
	
	@GetMapping
	public ResponseEntity<Set<RoleDTO>> getAll() {
		return ResponseEntity.ok(roleService.getAll());
	}
	
	@GetMapping("/{idRole}")
	public ResponseEntity<RoleDTO> getById(@PathVariable("idRole") final String idRole) {
		return ResponseEntity.ok(roleService.getById(idRole));
	}
	
	@PostMapping
	public ResponseEntity<RoleDTO> createNewRole(final CreateRoleDTO createDTO) {
		final RoleDTO newRole = roleService.create(createDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(newRole);
	}
	
	@PatchMapping("/{idRole}/assign/{idUser}")
	public ResponseEntity<String> assignRole(final String idRole, final String idUser) {
		userService.assignRole(idRole, idUser);
		return ResponseEntity.ok("Role assigned successfully.");
	}

}
