package com.gitlab.andersonbfn.priceline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PricelineSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricelineSpringApplication.class, args);
	}

}
