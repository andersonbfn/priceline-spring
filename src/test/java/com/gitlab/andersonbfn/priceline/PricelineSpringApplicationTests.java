package com.gitlab.andersonbfn.priceline;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.andersonbfn.priceline.dto.CreateRoleDTO;
import com.gitlab.andersonbfn.priceline.service.RoleService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class PricelineSpringApplicationTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	RoleService roleService;

	@Test
	void contextLoads() {
	}
	
	@Test
	void testRoleCreation() throws Exception {
		final CreateRoleDTO createRoleDTO = new CreateRoleDTO();
		createRoleDTO.setName("Role-Teste");
		
		mockMvc.perform(post("/roles")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(createRoleDTO)))
			.andExpect(status().isOk());
			
	}

}
